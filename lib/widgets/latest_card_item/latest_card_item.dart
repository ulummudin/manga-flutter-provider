import 'package:flutter/material.dart';
import 'package:manga_provider/models/latest_model.dart';
import 'package:manga_provider/pages/chapter/chapter_page.dart';
import 'package:manga_provider/pages/detail/detail_page.dart';

class LatestCardItem extends StatelessWidget {
  final LatestModel latest;

  const LatestCardItem(this.latest);

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context);
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => DetailPage(latest.slug),
          ),
        );
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.circular(8.0),
              ),
              clipBehavior: Clip.antiAlias,
              child: Stack(
                children: [
                  Image.network(
                    latest.thumbnailUrl,
                    width: double.infinity,
                    fit: BoxFit.cover,
                  ),
                  Positioned(
                    top: 4.0,
                    right: 4.0,
                    child: CircleAvatar(
                      radius: 13.0,
                      backgroundColor: Colors.grey[900],
                      foregroundColor: Colors.white,
                      child: Icon(Icons.favorite_outline, size: 14.0),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 4.0),
            height: media.size.height * (media.orientation == Orientation.portrait ? 0.08 : 0.15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  latest.title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(height: 4),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (_) => ChapterPage(latest.chapterSlug),
                      ),
                    );
                  },
                  child: Text(
                    latest.chapter,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 12.0),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
