import 'package:flutter/material.dart';
import 'package:manga_provider/models/detail_chapter_model.dart';
import 'package:manga_provider/pages/chapter/chapter_page.dart';

class ChapterListView extends StatelessWidget {
  final List<DetailChapterModel> chapters;

  const ChapterListView(this.chapters);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: chapters.length,
      itemBuilder: (context, index) {
        final chapter = chapters[index];
        return ListTile(
          title: Text(chapter.chapter),
          subtitle: Text(chapter.releaseDate),
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (_) => ChapterPage(chapter.slug),
              ),
            );
          },
        );
      },
    );
  }
}
