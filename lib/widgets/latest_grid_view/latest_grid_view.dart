import 'package:flutter/material.dart';
import 'package:manga_provider/models/latest_model.dart';
import 'package:manga_provider/widgets/latest_card_item/latest_card_item.dart';

class LatestGridView extends StatelessWidget {
  final List<LatestModel> latest;
  final ScrollController controller;

  const LatestGridView(
    this.latest, {
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      controller: controller,
      padding: const EdgeInsets.all(16.0),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 140.0,
        childAspectRatio: 0.6,
        mainAxisSpacing: 16.0,
        crossAxisSpacing: 16.0,
      ),
      itemCount: latest.length,
      itemBuilder: (context, index) => LatestCardItem(latest[index]),
    );
  }
}
