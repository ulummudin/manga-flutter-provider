import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class ChapterImageListView extends StatelessWidget {
  final List<String> images;
  final ScrollController controller;

  const ChapterImageListView(
    this.images, {
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      controller: controller,
      itemCount: images.length,
      itemBuilder: (context, index) {
        final image = images[index];
        return Stack(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: CircularProgressIndicator(),
              ),
            ),
            InteractiveViewer(
              child: FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                image: image,
              ),
            ),
          ],
        );
      },
    );
  }
}
