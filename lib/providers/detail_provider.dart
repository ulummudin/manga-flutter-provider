import 'package:flutter/foundation.dart';
import 'package:manga_provider/models/detail_model.dart';
import 'package:manga_provider/repositories/manga_repository.dart';

class DetailProvider with ChangeNotifier {
  final MangaRepository mangaRepository;

  DetailModel _detail = DetailModel();
  DetailModel get detail => _detail;

  DetailProvider(this.mangaRepository);

  void getDetail(String slug) async {
    final detail = await mangaRepository.getDetailManga(slug);
    _detail = detail;
    notifyListeners();
  }
}
