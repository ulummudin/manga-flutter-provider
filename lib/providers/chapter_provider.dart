import 'package:flutter/foundation.dart';
import 'package:manga_provider/models/chapter_model.dart';
import 'package:manga_provider/repositories/manga_repository.dart';

class ChapterProvider with ChangeNotifier {
  final MangaRepository mangaRepository;

  ChapterModel _chapter = ChapterModel();
  ChapterModel get chapter => _chapter;

  ChapterProvider(this.mangaRepository);

  void getChapter(String slug) async {
    final chapter = await mangaRepository.getChapterManga(slug);
    _chapter = chapter;
    notifyListeners();
  }
}
