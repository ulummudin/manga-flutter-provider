import 'package:flutter/foundation.dart';
import 'package:manga_provider/models/latest_model.dart';
import 'package:manga_provider/repositories/manga_repository.dart';

class LatestProvider with ChangeNotifier {
  final MangaRepository mangaRepository;

  List<LatestModel> _items = [];
  List<LatestModel> get items => _items;

  LatestProvider(this.mangaRepository);

  void getLatest() async {
    final latestItems = await mangaRepository.getLatestManga();
    _items = latestItems;
    notifyListeners();
  }
}
