class DisqusModel {
  final String title;
  final String url;
  final String postId;

  const DisqusModel({
    this.title = '',
    this.url = '',
    this.postId = '',
  });

  factory DisqusModel.fromJson(Map<String, dynamic> json) {
    return DisqusModel(
      title: json['title'] as String,
      url: json['url'] as String,
      postId: json['postId'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': this.title,
      'url': this.url,
      'postId': this.postId,
    };
  }
}
