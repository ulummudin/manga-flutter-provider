class SlugModel {
  final String title;
  final String slug;

  const SlugModel({
    this.title = '',
    this.slug = '',
  });

  factory SlugModel.fromJson(Map<String, dynamic> json) {
    return SlugModel(
      title: json['title'] as String,
      slug: json['slug'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': this.title,
      'slug': this.slug,
    };
  }
}
