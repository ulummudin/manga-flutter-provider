class LatestModel {
  final String title;
  final String slug;
  final String thumbnailUrl;
  final String type;
  final String date;
  final String chapter;
  final String chapterSlug;

  const LatestModel({
    this.title = '',
    this.slug = '',
    this.thumbnailUrl = '',
    this.type = '',
    this.date = '',
    this.chapter = '',
    this.chapterSlug = '',
  });

  factory LatestModel.fromJson(Map<String, dynamic> json) {
    return LatestModel(
      title: json['title'] as String,
      slug: json['slug'] as String,
      thumbnailUrl: json['thumbnailUrl'] as String,
      type: json['type'] as String,
      date: json['date'] as String,
      chapter: json['chapter'] as String,
      chapterSlug: json['chapterSlug'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': this.title,
      'slug': this.slug,
      'thumbnailUrl': this.thumbnailUrl,
      'type': this.type,
      'date': this.date,
      'chapter': this.chapter,
      'chapterSlug': this.chapterSlug,
    };
  }
}
