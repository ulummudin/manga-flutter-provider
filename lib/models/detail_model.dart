import 'package:manga_provider/models/detail_chapter_model.dart';
import 'package:manga_provider/models/disqus_model.dart';
import 'package:manga_provider/models/slug_model.dart';

class DetailModel {
  final String title;
  final String imageUrl;
  final String synopsis;
  final List<SlugModel> genres;
  final List<DetailChapterModel> chapters;
  final DisqusModel disqus;

  const DetailModel({
    this.title = '',
    this.imageUrl = '',
    this.synopsis = '',
    this.genres = const [],
    this.chapters = const [],
    this.disqus = const DisqusModel(),
  });

  factory DetailModel.fromJson(Map<String, dynamic> json) {
    return DetailModel(
      title: json['title'] as String,
      imageUrl: json['imageUrl'] as String,
      synopsis: json['synopsis'] as String,
      genres: (json['genres'] as List)?.map((e) => e == null ? null : SlugModel.fromJson(e as Map<String, dynamic>))?.toList(),
      chapters: (json['chapters'] as List)?.map((e) => e == null ? null : DetailChapterModel.fromJson(e as Map<String, dynamic>))?.toList(),
      disqus: json['disqus'] == null ? null : DisqusModel.fromJson(json['disqus'] as Map<String, dynamic>),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': this.title,
      'imageUrl': this.imageUrl,
      'synopsis': this.synopsis,
      'genres': this.genres,
      'chapters': this.chapters,
      'disqus': this.disqus,
    };
  }
}
