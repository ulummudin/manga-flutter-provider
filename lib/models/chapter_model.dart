import 'package:manga_provider/models/disqus_model.dart';

class ChapterModel {
  final String title;
  final String prevChapterSlug;
  final String nextChapterSlug;
  final List<String> images;
  final DisqusModel disqus;

  const ChapterModel({
    this.title = '',
    this.prevChapterSlug = '',
    this.nextChapterSlug = '',
    this.images = const [],
    this.disqus = const DisqusModel(),
  });

  factory ChapterModel.fromJson(Map<String, dynamic> json) {
    return ChapterModel(
      title: json['title'] as String,
      prevChapterSlug: json['prevChapterSlug'] as String,
      nextChapterSlug: json['nextChapterSlug'] as String,
      images: (json['images'] as List)?.map((e) => e as String)?.toList(),
      disqus: json['disqus'] == null ? null : DisqusModel.fromJson(json['disqus'] as Map<String, dynamic>),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': this.title,
      'prevChapterSlug': this.prevChapterSlug,
      'nextChapterSlug': this.nextChapterSlug,
      'images': this.images,
      'disqus': this.disqus,
    };
  }
}
