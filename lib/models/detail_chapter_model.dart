class DetailChapterModel {
  final String chapter;
  final String slug;
  final String releaseDate;

  const DetailChapterModel({
    this.chapter = '',
    this.slug = '',
    this.releaseDate = '',
  });

  factory DetailChapterModel.fromJson(Map<String, dynamic> json) {
    return DetailChapterModel(
      chapter: json['chapter'] as String,
      slug: json['slug'] as String,
      releaseDate: json['releaseDate'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'chapter': this.chapter,
      'slug': this.slug,
      'releaseDate': this.releaseDate,
    };
  }
}
