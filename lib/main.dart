import 'package:flutter/material.dart';
import 'package:manga_provider/pages/latest/latest_page.dart';
import 'package:manga_provider/providers/chapter_provider.dart';
import 'package:manga_provider/providers/detail_provider.dart';
import 'package:manga_provider/providers/latest_provider.dart';
import 'package:manga_provider/repositories/manga_repository.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mangaRepository = MangaRepository();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: LatestProvider(mangaRepository)..getLatest(),
        ),
        ChangeNotifierProvider.value(value: DetailProvider(mangaRepository)),
        ChangeNotifierProvider.value(value: ChapterProvider(mangaRepository)),
      ],
      child: MaterialApp(
        title: 'Manga',
        theme: ThemeData.dark().copyWith(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: LatestPage(),
      ),
    );
  }
}
