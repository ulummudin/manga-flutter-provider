import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:manga_provider/models/chapter_model.dart';
import 'package:manga_provider/models/detail_model.dart';
import 'package:manga_provider/models/latest_model.dart';

class MangaRepository {
  final baseUrl = 'http://api.manganime.ulummudin.xyz/v2/manga';
  final client = http.Client();

  static List<LatestModel> _parsedLatest(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<LatestModel>((json) => LatestModel.fromJson(json)).toList();
  }

  Future<List<LatestModel>> getLatestManga({String page = '1'}) async {
    final response = await client.get(
      '$baseUrl/latest?page=$page',
    );
    return _parsedLatest(response.body);
  }

  Future<DetailModel> getDetailManga(String slug) async {
    final response = await client.get(
      '$baseUrl/detail/$slug',
    );
    final data = json.decode(response.body);
    return DetailModel.fromJson(data);
  }

  Future<ChapterModel> getChapterManga(String slug) async {
    final response = await client.get(
      '$baseUrl/chapter/$slug',
    );
    final data = json.decode(response.body);
    return ChapterModel.fromJson(data);
  }
}
