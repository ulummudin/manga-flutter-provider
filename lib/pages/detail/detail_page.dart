import 'package:flutter/material.dart';
import 'package:manga_provider/providers/detail_provider.dart';
import 'package:manga_provider/widgets/chapter_list_view/chapter_list_view.dart';
import 'package:provider/provider.dart';

class DetailPage extends StatefulWidget {
  final String slug;

  const DetailPage(this.slug);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  void initState() {
    super.initState();

    Provider.of<DetailProvider>(context, listen: false).getDetail(widget.slug);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Consumer<DetailProvider>(
          builder: (context, detail, child) {
            return Text(detail.detail.title);
          },
        ),
      ),
      body: Consumer<DetailProvider>(
        builder: (context, detail, child) {
          return ChapterListView(detail.detail.chapters);
        },
      ),
    );
  }
}
