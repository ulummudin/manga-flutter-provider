import 'package:flutter/material.dart';
import 'package:manga_provider/providers/chapter_provider.dart';
import 'package:manga_provider/widgets/chapter_image_list_view/chapter_image_list_view.dart';
import 'package:provider/provider.dart';

class ChapterPage extends StatefulWidget {
  final String slug;

  const ChapterPage(this.slug);

  @override
  _ChapterPageState createState() => _ChapterPageState();
}

class _ChapterPageState extends State<ChapterPage> {
  @override
  void initState() {
    super.initState();

    Provider.of<ChapterProvider>(context, listen: false).getChapter(widget.slug);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Consumer<ChapterProvider>(
          builder: (context, chapter, child) {
            return Text(chapter.chapter.title);
          },
        ),
      ),
      body: Consumer<ChapterProvider>(
        builder: (context, chapter, child) {
          return ChapterImageListView(chapter.chapter.images);
        },
      ),
    );
  }
}
