import 'package:flutter/material.dart';
import 'package:manga_provider/providers/latest_provider.dart';
import 'package:manga_provider/widgets/latest_grid_view/latest_grid_view.dart';
import 'package:provider/provider.dart';

class LatestPage extends StatefulWidget {
  @override
  _LatestPageState createState() => _LatestPageState();
}

class _LatestPageState extends State<LatestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manga'),
      ),
      body: Consumer<LatestProvider>(
        builder: (context, provider, child) {
          final items = provider.items;
          return LatestGridView(items);
        },
      ),
    );
  }
}
